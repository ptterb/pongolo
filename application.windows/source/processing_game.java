import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.Dimension; 
import java.awt.Frame; 
import java.awt.event.MouseEvent; 
import java.awt.event.KeyEvent; 
import java.awt.event.FocusEvent; 
import java.awt.Image; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class processing_game extends PApplet {


/**



*/
int wwidth = 500;
int wheight = 500;
int paddle = 50;
int x = wwidth/2 - paddle/2;
int y = wheight-20;
int ex = PApplet.parseInt(random(0,wwidth));
int ey = 30;
int total = 0;
int speedx = 3;
int speedy = 5;
int paddleSpeed = 5;
boolean posx = true;
boolean posy = true;
boolean gameon = true;

public void setup() {
   size(wwidth,wheight);
   noStroke();
   background(0);
   
  
} 

public void draw(){
  background(0);
  
  if (gameon == true) {
                
        if(keyPressed == true) {
          if (key == 'a') {
               x = x-paddleSpeed;
            }
            else if (key == 'w') {
               y = y-paddleSpeed;
            } 
            else if (key == 's') {
               y = y+paddleSpeed;
            }
            else if (key == 'd') {
               x = x+paddleSpeed;
            }
            if (x >= wwidth - paddle) {
              x = wwidth - paddle;
            }
            if (x <= 0) {
              x = 0;
            }
            rect(x,y,paddle,10);
        }
          if (posx == true) {
            ex = ex +speedx;
          } else {
            ex = ex -speedx;
          }
          
          if (posy == true) {
            ey = ey +speedy;
          } else {
            ey = ey -speedy;
          }
          ellipse(ex,ey,30,30);
          if (ex >= wwidth-15 || ex <= 15 ) {
            posx = !posx;
          }
          if (ey <= 15 ) {
            posy = !posy;
          }
          if (ey >= wheight-25) {
            if (ex >= x && ex <= x+paddle) {
              posy = !posy;
              total = total +1;
              if (total % 5 == 0) {
                speedx = speedx +1;
                speedy = speedy + 1;
              }
              if (total % 5 == 0) {
                paddleSpeed = paddleSpeed +1;
              }
            }
            else {
             gameon = false;
            }
          }
          
  }
  else {
       text("GAME OVER! Press n for new game", wheight/2-100, 20);
     }
    rect(x,y,paddle,10);
     text(total,20,20);
}

public void keyPressed() {
    if (key == ' ') {
        rect(10,10,10,10);
    }    
    if (key == 'n') {
        total = 0;
        speedx = 3;
        speedy = 5;
        paddleSpeed = 5;
        gameon = true;
        ex = PApplet.parseInt(random(0,wwidth));
        ey = 30;
    }
}
  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#FFFFFF", "processing_game" });
  }
}
