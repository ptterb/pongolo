
/**
Pongolo

Author: Brett Peterson

Pongolo is a solo version of Pong. 
This is my first Processing application and is used as an example

Upcoming Feature List:
  -High Scores

*/

// Variables to make the game 'graphics' scale correctly
int wwidth = 500; // Window width
int wheight = 500; // Window height
int paddle = 50; // Paddle width
int x = wwidth/2 - paddle/2; // Initial paddle starting point
int y = wheight-20; // Paddle y-axis
int diam = 30; // Diameter of ellipse
int ex = int(random(0,wwidth)); // Ellipse random start point
int ey = 30; // Ellipse y-axis start

int total = 0;  // Score
int speedx = 3; // Starting speed of ellipse x-axis
int speedy = 5; // Starting speed of ellipse y-axis 
int paddleSpeed = 5; // Speed of player's paddle
boolean posx = true; // Used to check direction of Ellipse
boolean posy = true; // Used to check direction of Ellipse
int gameon = 0;  // Is the main game loop running? Used for Start and Game Over screens

void setup() {
   size(wwidth,wheight);
   noStroke();
   background(0);
   
  
} 

void draw(){
  background(0);
  
  // Main game loop
  if (gameon == 1) {
        // Score in top left corner
        textSize(14);
        text(total,20,20);
        
        // Check for keyboard input and move paddle accordingly
        if(keyPressed == true) {
          if (key == 'a') {
               x = x-paddleSpeed;
            }
            else if (key == 'd') {
               x = x+paddleSpeed;
            }
            // Check paddle against width of window to not exceed
            if (x >= wwidth - paddle) {
              x = wwidth - paddle;
            }
            if (x <= 0) {
              x = 0;
            }
            
            rect(x,y,paddle,10);
        }
          // Move ellipse in proper direction        
          if (posx == true) {
            ex = ex +speedx;
          } else {
            ex = ex -speedx;
          }
          if (posy == true) {
            ey = ey +speedy;
          } else {
            ey = ey -speedy;
          }
          ellipse(ex,ey,diam,diam);
          
          // Make sure ellipse doesn't go outside window, reverse direction
          if (ex >= wwidth-diam/2 || ex <= diam/2 ) {
            posx = !posx;
          }
          if (ey <= diam/2 ) {
            posy = !posy;
          }
          // When ellipse is at gets to the paddle level, check to see if caught
          if (ey >= wheight-25) {
            // Check to see if inside paddle
            if (ex >= x && ex <= x+paddle) {
              posy = !posy;
              total +=1; // Add to score
              
              // Increase difficulty every 5 points
              if (total % 5 == 0) {
                speedx += 1;
                speedy += 1;
                paddleSpeed += 1;
              }
            }
            // If player missed ellipse, end game
            else {
             gameon = 2;
            }
          }
          
  }
  
  // End Game Screen. If adjusting the size of the window,
  // adjust the text sizes accordingly
  else if (gameon == 2) {
       textSize(20);
       text(total,wwidth/2-20,100);
       textSize(40);
       text("GAME OVER!", wwidth/2-130, 150);
       textSize(14);
       text("Press 'N' for new game", wwidth/2-90, 170);
     }
  // Start Game screen   
  else if (gameon == 0) {
       textSize(40);
       text("PONGOLO", wwidth/2-100, 100);
       textSize(16);
       text("Press 'N' to start a new game 'A' moves left 'D' moves right", wwidth/2-230, 150);
       textSize(12);
       text("2011 Brett Peterson SubmergedSpaceman.com", wwidth/2-150, 200);
       textSize(14);
       text(total,20,20);
  }
    rect(x,y,paddle,10);
     
}

// Check for 'N' keypress at any time to start a new game
void keyPressed() {
    
    if (key == 'n') {
        // Reset game variables
        total = 0;
        speedx = 3;
        speedy = 5;
        paddleSpeed = 5;
        gameon = 1;
        ex = int(random(0,wwidth));
        ey = 30;
    }
}
