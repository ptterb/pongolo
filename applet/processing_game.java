import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.Dimension; 
import java.awt.Frame; 
import java.awt.event.MouseEvent; 
import java.awt.event.KeyEvent; 
import java.awt.event.FocusEvent; 
import java.awt.Image; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class processing_game extends PApplet {


/**



*/
int wwidth = 500;
int wheight = 500;
int paddle = 50;
int x = wwidth/2 - paddle/2;
int y = wheight-20;
int ex = 30;
int ey = 30;
int total = 0;
boolean posx = true;
boolean posy = true;

public void setup() {
   size(wwidth,wheight);
   noStroke();
   background(0);
   
  
} 

public void draw(){
  background(0);
  text(total,20,20);
  
  if(keyPressed == true) {
    if (key == 'a') {
         x = x-5;
      }
      else if (key == 'w') {
         y = y-5;
      } 
      else if (key == 's') {
         y = y+5;
      }
      else if (key == 'd') {
         x = x+5;
      }
      if (x >= wwidth - paddle) {
        x = wwidth - paddle;
      }
      if (x <= 0) {
        x = 0;
      }
      rect(x,y,paddle,10);
  }
    if (posx == true) {
      ex = ex +3;
    } else {
      ex = ex -3;
    }
    
    if (posy == true) {
      ey = ey +5;
    } else {
      ey = ey -5;
    }
    ellipse(ex,ey,30,30);
    if (ex >= wwidth-15 || ex <= 15 ) {
      posx = !posx;
    }
    if (ey <= 15 ) {
      posy = !posy;
    }
    if (ey >= wheight-25) {
      if (ex >= x && ex <= x+paddle) {
        posy = !posy;
        total = total +1;
      }
      else {
        print("GAME OVER");
      }
    }
    rect(x,y,paddle,10);
}

public void keyPressed() {
    if (key == ' ') {
        rect(10,10,10,10);
    }    
}
  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#FFFFFF", "processing_game" });
  }
}
